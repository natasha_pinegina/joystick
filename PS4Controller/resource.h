﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется PS4Controller.rc
//
#define IDD_PS4CONTROLLER_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_PICTURE              1000
#define IDC_RIGHT_STICK_PICTURE         1001
#define IDC_LEFT_STICK_PICTURE          1002
#define IDC_CROSSHAIR                   1003
#define IDC_FIGURES_BUTTONS             1004
#define IDC_BUTTON_CONNECT              1005
#define IDC_BUTTON_DISCONNECT           1006
#define IDC_SLIDER_LEFT                 1008
#define IDC_SLIDER_LEFT2                1009
#define IDC_SLIDER_RIGHT                1009
#define IDC_GYROXYZ                     1010
#define IDC_ACCELXYZ                    1011
#define IDC_TPad                        1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
