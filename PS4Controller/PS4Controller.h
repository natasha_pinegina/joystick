﻿
// PS4Controller.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CPS4ControllerApp:
// Сведения о реализации этого класса: PS4Controller.cpp
//

class CPS4ControllerApp : public CWinApp
{
public:
	CPS4ControllerApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CPS4ControllerApp theApp;
