#pragma once

#include <iostream>
#include <iomanip>
#include "libusb.h"

#define VID 0x054C
#define PID 0x09CC
#define BULK_EP_OUT     0x84
#define BULK_EP_IN      0x03

using namespace std;

/*        count = libusb_get_device_list(context, &list);
        assert(count > 0);

        for (size_t idx = 0; idx < count; ++idx) {
            libusb_device* device = list[idx];
            libusb_device_descriptor desc = { 0 };

            rc = libusb_get_device_descriptor(device, &desc);
            assert(rc == 0);

            if (desc.idVendor == 0x054c)
            {
                vendorDS4 = desc.idVendor;
                productDS4 = desc.idProduct;
            }

            printf("Vendor:Device = %04x:%04x\n", desc.idVendor, desc.idProduct);
        }*/

enum DPADState
{
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW
};

struct DS4State
{
    bool ps_button;
    int counter;
    bool tpad_click;
    bool up, down, left, right;
    bool cross, triangle, square, circle;
    int battery_Level;
    int left_stick_axis_x;
    int left_stick_axis_y;
    int right_stick_axis_x;
    int right_stick_axis_y;
    bool left_stick_click;
    bool right_stick_click;
    int L2;
    int L1R1;
    string Hammers;
    int R2;
    int GyroX;
    int GyroY;
    int GyroZ;

    int AccelX;
    int AccelY;
    int AccelZ;
    int Typeface;
    int headphones;
    int bluetooth;

    int T_PAD;
    int T_PAD1;
    string t_pad1;
    int T_PAD2;
    int Koord_finger1, Koord_finger2, Koord_finger3;
    int Koord_finger11, Koord_finger21, Koord_finger31;
    int xx = 0, yy = 0, x = 0, y = 0;
    const unsigned int lowMask1 = 0xF0;
    const unsigned int lowMask2 = 0x0F;

    int share;
    string Share;
    DPADState dpad;

    int rumble_l = 0,
        rumble_r = 0,
        r = 0,
        g = 0,
        b = 50,
        crc = 0,
        volume = 50,
        flash_bright = 150,
        flash_dark = 150;
    
    void print()
    {        
        cout << "PS Button " << (ps_button ? "is down" : "is up") << endl;
        cout << "Counter=" << counter << endl;
        cout << "Tpad click " << (tpad_click ? "is down" : "is up") << endl;
        cout << "Share/Options" << "\t" << Share << "\t" << "Value=" << share << endl;
        cout << "Cross " << (cross ? "is down" : "is up") << "; ";
        cout << "Triangle " << (triangle ? "is down" : "is up") << "; ";
        cout << "Square " << (square ? "is down" : "is up") << "; ";
        cout << "Circle " << (circle ? "is down" : "is up") << "; ";
        cout << endl;
        cout << "DPADState " << dpad << "; ";
        cout << endl;
        cout << "Battery Level = " << battery_Level / 255.0 * 100 << "%" << endl;
        cout << "Left-X = " << dec << left_stick_axis_x << "\t"
            << "Left-Y = " << left_stick_axis_y << "\t"
            << "Right-X = " << right_stick_axis_x << "\t"
            << "Right-Y = " << right_stick_axis_y << "\t" << endl;

        cout << "Hammers:" << "\t" << Hammers << ";" << "\n" << "L2 = " << L2 << "; " << "\t" << "R2 = " << R2 << endl;
        cout << endl;
        cout << "GyroX:" << "\t" << GyroX << endl;
        cout << "GyroY:" << "\t" << GyroY << endl;
        cout << "GyroZ:" << "\t" << GyroZ << endl;
        cout << endl;
        cout << "AccelX:" << "\t" << AccelX << endl;
        cout << "AccelY:" << "\t" << AccelY << endl;
        cout << "AccelZ:" << "\t" << AccelZ << endl;
        cout << endl;

        cout << "Typeface " << (Typeface ? "connected" : "not connected") << "; ";
        cout << "headphones " << (headphones ? "connected" : "not connected") << "; ";
        cout << "bluetooth " << (bluetooth ? "connected" : "not connected") << "; " << endl;
        cout << endl;
        cout << "T_PAD" << "\t" << T_PAD << "\t" << endl;
        cout << "Number of touches for 1 finger:" << "\t" << t_pad1 << T_PAD1 << "\t" << endl;
        cout << "Number of touches for 2 finger:" << "\t" << T_PAD2 << "\t" << endl;
        cout << endl;
        cout << "The coordinates of the fingers probably:" << "\t" << Koord_finger1 <<
            "\t" << Koord_finger2 << "\t" << Koord_finger3 << endl;

        /*state.ps_button = data[7] & 0x1;
        state.counter = data[7] >> 2;
        state.tpad_click = data[7] & 0x2;*/
        /*cout << "GyroX:" << "\t" << GyroX << "\t";
        cout << "GyroY:" << "\t" << GyroY << "\t";
        cout << "GyroZ:" << "\t" << GyroZ << "\t";
        cout << endl;
        cout << "AccelX:" << "\t" << AccelX << "\t";
        cout << "AccelY:" << "\t" << AccelY << "\t";
        cout << "AccelZ:" << "\t" << AccelZ << "\t";
        cout << endl;*/
    }
};

class DS4Controller
{
public:
    ~DS4Controller()
    {
        int r = 0;
        r = libusb_release_interface(handle, 0);
        if (r != 0)
        {
            cout << "Something wrong in libusb_release_interface" << endl;
            return;
        }
        libusb_close(handle);
        libusb_exit(context);
    }

	void Connect()
	{
        int r = 0;

        r = libusb_init(&context);
        if (r < 0)
        {
            cout << "Something wrong" << endl;
            return;
        }

        handle = libusb_open_device_with_vid_pid(context, VID, PID);
        if (handle == NULL)
        {
            printf("\nError in device opening!");
        }
        else
            cout << "Device Opened!" << endl;

       
        if (libusb_kernel_driver_active(handle, 0) == 1)
        {
            printf("\nKernel Driver Active");
            if (libusb_detach_kernel_driver(handle, 0) == 0)
                printf("\nKernel Driver Detached!");
            else
            {
                printf("\nCouldn't detach kernel driver!\n");
            }
        }

        r = libusb_claim_interface(handle, 3);
        if (r < 0)
        {
            printf("\nCannot Claim Interface\n");
            cout << libusb_error_name(r) << endl;
        }
        else
            printf("\nClaimed Interface\n");
	}

	void Update()
	{
        int r = 0;
        int received = 0;
        r = libusb_bulk_transfer(handle, BULK_EP_OUT, data, 64, &received, 0);
        if (r != 0)
        {
            cout << libusb_error_name(r) << endl;
            printf("\nError in read! r = %d and received = %d\n", r, data);
        }
        Parse();
        make_ya_ne_znay_chto_yeto_za_data();
	}

	DS4State GetState()
	{
		return state;
	}
private:
    void Parse()
    {
        state.cross = data[5] & 0x20;
        state.triangle = data[5] & 0x80;
        state.square = data[5] & 0x10;
        state.circle = data[5] & 0x40;
        
        state.left_stick_axis_x = (int)data[1];
        state.left_stick_axis_y = (int)data[2];
        state.right_stick_axis_x = (int)data[3];
        state.right_stick_axis_y = (int)data[4];

        state.battery_Level = (int)data[12];

        state.L1R1 = (int)data[6];
        if (state.L1R1 == 0)  state.Hammers = "The trigger is not clamped";
        if (state.L1R1 == 1)  state.Hammers = "The left trigger is clamped";
        if (state.L1R1 == 2)  state.Hammers = "The right trigger is clamped";
        if (state.L1R1 == 3)  state.Hammers = "Both triggers are clamped";

        //state.R1 = (int)data[7];
        state.L2 = (int)data[8];
        state.R2 = (int)data[9];

        state.left_stick_click = data[6] & 0x40;
        state.right_stick_click = data[6] & 0x80;

        state.GyroX = data[14] << 8 | data[13];
        state.GyroX = perfectTransform(state.GyroX);
        state.GyroY = data[16] << 8 | data[15];
        state.GyroY = perfectTransform(state.GyroY);
        state.GyroZ = data[18] << 8 | data[17];
        state.GyroZ = perfectTransform(state.GyroZ);


        state.AccelX = data[20] << 8 | data[19];
        state.AccelX = perfectTransform(state.AccelX);
        state.AccelY = data[22] << 8 | data[21];
        state.AccelY = perfectTransform(state.AccelY);
        state.AccelZ = data[24] << 8 | data[23];
        state.AccelZ = perfectTransform(state.AccelZ);

        state.Typeface = (int)data[30] & 0x7B;
        state.bluetooth = (int)data[30] & 0x08;
        state.headphones = (int)data[30] & 0x3B;

        state.T_PAD = (int)data[34];
        state.T_PAD1 = (int)data[35];
        if (state.T_PAD1 == 0) state.t_pad1 = "Touch number:";
        else state.t_pad1 = "You let go of your finger:";
        state.T_PAD2 = (int)data[39];

        state.Koord_finger1 = (int)data[40];
        state.Koord_finger2 = (int)data[41];
        state.Koord_finger3 = (int)data[42];

        state.y = state.Koord_finger3 | (state.Koord_finger2 & state.lowMask1);
        state.y = perfectTransformTPAD(state.y);
        state.x = (state.Koord_finger2 & state.lowMask2) | state.Koord_finger1;
        state.x = perfectTransformTPAD(state.x);

        state.Koord_finger11 = (int)data[36];
        state.Koord_finger21 = (int)data[37];
        state.Koord_finger31 = (int)data[38];

        state.yy = state.Koord_finger31 | (state.Koord_finger21 & state.lowMask1);
        state.yy = perfectTransformTPAD(state.yy);
        state.xx = (state.Koord_finger21 & state.lowMask2) | state.Koord_finger11;
        state.xx = perfectTransformTPAD(state.xx);

        state.share = (int)data[6];
        if (state.share == 16) state.Share = "share";
        if (state.share == 32) state.Share = "options";
        if (state.share == 0) state.Share = "nothing";

        state.dpad = (DPADState)(data[5] & 0xF);
        state.ps_button = data[7] & 0x1;
        state.counter = data[7] >> 2;
        state.tpad_click = data[7] & 0x2;
    }

    void make_ya_ne_znay_chto_yeto_za_data()
    {
        //��� data
        ya_ne_znay_chto_yeto_za_data[0] = 0;
        //��� ��������
        ya_ne_znay_chto_yeto_za_data[1] = 0;

        ya_ne_znay_chto_yeto_za_data[4] = 0;
        ya_ne_znay_chto_yeto_za_data[5] = 0;
        ya_ne_znay_chto_yeto_za_data[6] = 0;
        //����-����.
        ya_ne_znay_chto_yeto_za_data[7] = 0;
        ya_ne_znay_chto_yeto_za_data[8] = 0;
        // R
        ya_ne_znay_chto_yeto_za_data[9] = 0;
        // G
        ya_ne_znay_chto_yeto_za_data[10] = 0;
        // B
        ya_ne_znay_chto_yeto_za_data[11] = 0;
        // volume
        ya_ne_znay_chto_yeto_za_data[25] = 0;

        /*	data = [0xf3,0x4,0x00]
        data.extend([rumble_l,rumble_r,r,g,b,flash_bright,flash_dark])
        data.extend([0]*8)
        data.extend([0x43,0x43,0x00,volume,0x85])
        */
    }

    int perfectTransform(int in)
    {
        if (in > pow(2, 15))
        {
            return in - pow(2, 15);
        }
        else
        {
            return in + pow(2, 15);
        }
    }

    int perfectTransformTPAD(int in)
    {
        if (in > pow(2, 4))
        {
            return in - pow(2, 4);
        }
        else
        {
            return in + pow(2, 4);
        }
    }

	DS4State state;
    unsigned char data[64];
    unsigned char ya_ne_znay_chto_yeto_za_data[78];
    libusb_context* context = NULL;
    libusb_device_handle* handle = NULL;
};