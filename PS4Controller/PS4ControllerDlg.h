﻿
// PS4ControllerDlg.h: файл заголовка
//

#pragma once

#include "DS4ControllerSource.h"
#include <vector>

// Диалоговое окно CPS4ControllerDlg
class CPS4ControllerDlg : public CDialogEx
{
// Создание
public:
	CPS4ControllerDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PS4CONTROLLER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	DWORD dwThread;
	HANDLE hThread;

	bool isConnected = false;

	// ** Picture-LeftStick **
	CWnd* PicWndLeftStick;
	CDC* PicDcLeftStick;
	CRect PicLeftStick;
	//\\//\\//\\

	// ** Picture-RightStick **
	CWnd* PicWndRightStick;
	CDC* PicDcRightStick;
	CRect PicRightStick;
	//\\//\\//\\

	// ** Picture-Crosshair **
	CWnd* PicWndCrosshair;
	CDC* PicDcCrosshair;
	CRect PicCrosshair;
	//\\//\\//\\

	// ** Picture-FiguresButton **
	CWnd* PicWndFiguresButton;
	CDC* PicDcFiguresButton;
	CRect PicFiguresButton;
	//\\//\\//\\

	// ** Picture-ArrowButton **
	CWnd* PicWndArrowButton;
	CDC* PicDcArrowButton;
	CRect PicArrowButton;
	//\\//\\//\\

	// ** Picture-Buttons **
	CWnd* PicWndButtons;
	CDC* PicDcButtons;
	CRect PicButtons;
	//\\//\\//\\

	// ** Picture-Buttons **
	CWnd* PicWndGyro;
	CDC* PicDcGyro;
	CRect PicGyro;

	// ** Picture-Buttons **
	CWnd* PicWndAccel;
	CDC* PicDcAccel;
	CRect PicAccel;

	// ** TPAD **
	CWnd* PicWndTPAD;
	CDC* PicDcTPAD;
	CRect PicTPAD;

	// ** Button **
	CWnd* PicWndButton;
	CDC* PicDcButton;
	CRect PicButton;

	//\\//\\//\\

	// ** DOTS/Scale **
	double xp_left = 0, yp_left = 0,				//коэфициенты пересчета
		xmin_left = 0, xmax_left = 0,				//максисимальное и минимальное значение х 
		ymin_left = 0, ymax_left = 0;				//максисимальное и минимальное значение y

	double xp_right = 0, yp_right = 0,				//коэфициенты пересчета
		xmin_right = 0, xmax_right = 0,				//максисимальное и минимальное значение х 
		ymin_right = 0, ymax_right = 0;				//максисимальное и минимальное значение y

	double xp_but = 0, yp_but = 0,				//коэфициенты пересчета
		xmin_but = 0, xmax_but = 0,				//максисимальное и минимальное значение х 
		ymin_but = 0, ymax_but = 0;				//максисимальное и минимальное значение y

	double xp_cross = 0, yp_cross = 0,				//коэфициенты пересчета
		xmin_cross = 0, xmax_cross = 0,				//максисимальное и минимальное значение х 
		ymin_cross = 0, ymax_cross = 0;				//максисимальное и минимальное значение y

	double xp_figures = 0, yp_figures = 0,				//коэфициенты пересчета
		xmin_figures = 0, xmax_figures = 0,				//максисимальное и минимальное значение х 
		ymin_figures = 0, ymax_figures = 0;				//максисимальное и минимальное значение y

	double xp_arrow = 0, yp_arrow = 0,				//коэфициенты пересчета
		xmin_arrow = 0, xmax_arrow = 0,				//максисимальное и минимальное значение х 
		ymin_arrow = 0, ymax_arrow = 0;				//максисимальное и минимальное значение y

	double xpgyro = 0, ypgyro = 0,				//коэфициенты пересчета
		xmingyro = 0, xmaxgyro = 0,				//максисимальное и минимальное значение х 
		ymingyro = 0, ymaxgyro = 0;				//максисимальное и минимальное значение y

	double xpaccel = 0, ypaccel = 0,				//коэфициенты пересчета
		xminaccel = 0, xmaxaccel = 0,				//максисимальное и минимальное значение х 
		yminaccel = 0, ymaxaccel = 0;				//максисимальное и минимальное значение y

	double xp_button = 0, yp_button = 0,				//коэфициенты пересчета
		xmin_button = 0, xmax_button = 0,				//максисимальное и минимальное значение х 
		ymin_button = 0, ymax_button = 0;				//максисимальное и минимальное значение y

	double xp_T_pad = 0, yp_T_pad = 0,				//коэфициенты пересчета
		xmin_T_pad = 0, xmax_T_pad = 0,				//максисимальное и минимальное значение х 
		ymin_T_pad = 0, ymax_T_pad = 0;				//максисимальное и минимальное значение y
	//\\//\\//\\

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	// ** Methods ** 
	void Mashtab(double arr[], int dim, double* mmin, double* mmax);
	void DrawPosForStick(CDC* WinDc, CRect WinPic, double x, double y, bool isClick);
	void DrawStatusFigures(CDC* WinDc, CRect WinPic, bool cross, bool trian, bool circle, bool rectan);
	void DrawStatusArrow(CDC* WinDc, CRect WinPic, DPADState updownleftright);
	void DrawGyro(vector<int>& x, vector<int>& y, vector<int>& z, CDC* WinDc, CRect WinxmaxGraphc);
	void DrawAccel(vector<int>& x, vector<int>& y, vector<int>& z, CDC* WinDc, CRect WinxmaxGraphc);
	void TPAD(int x, int y, int xx, int yy, CDC* WinDc, CRect WinPic);
	void DrawButtons(CDC* WinDc, CRect WinPic, int isClick, bool isPs, int L1R1);
	void ReadFunction();
	afx_msg void OnBnClickedButtonConnect();
	afx_msg void OnBnClickedButtonDisconnect();

	DS4Controller ctrl;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	BOOL bRunTh = false;

	CSliderCtrl progress_left_trigger;
	CSliderCtrl progress_right_trigger;

	vector<int> vecGyroX;
	vector<int> vecGyroY;
	vector<int> vecGyroZ;

	vector<int> vecAccelX;
	vector<int> vecAccelY;
	vector<int> vecAccelZ;
};
