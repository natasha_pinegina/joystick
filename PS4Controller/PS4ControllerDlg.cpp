﻿
// PS4ControllerDlg.cpp: файл реализации
//
#include "pch.h"
#include "framework.h"
#include "PS4Controller.h"
#include "PS4ControllerDlg.h"
#include "afxdialogex.h"
#include "libusb.h"

#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DOTS_STICK(x,y) (xp_left * ((x)-xmin_left)), (yp_left * ((y)-ymax_left))
#define DOTS_FIGURES(x,y) (xp_figures * ((x)-xmin_figures)), (yp_figures * ((y)-ymax_figures))
#define DOTS_ARROW(x,y) (xp_arrow * ((x)-xmin_arrow)), (yp_arrow * ((y)-ymax_arrow))
#define DOTS_GYRO(x,y) (xpgyro * ((x)-xmingyro)), (ypgyro * ((y)-ymaxgyro))
#define DOTS_ACCEL(x,y) (xpaccel * ((x)-xminaccel)), (ypaccel * ((y)-ymaxaccel))
#define DOTS_BUTTONS(x,y) (xp_button * ((x)-xmin_button)), (yp_button * ((y)-ymax_button))
#define DOTS_TPAD(x,y) (xp_T_pad * ((x)-xmin_T_pad)), (yp_T_pad * ((y)-ymax_T_pad))

using namespace std;

CPS4ControllerDlg::CPS4ControllerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PS4CONTROLLER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPS4ControllerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_LEFT, progress_left_trigger);
	DDX_Control(pDX, IDC_SLIDER_RIGHT, progress_right_trigger);
}

BEGIN_MESSAGE_MAP(CPS4ControllerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CPS4ControllerDlg::OnBnClickedButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CPS4ControllerDlg::OnBnClickedButtonDisconnect)
END_MESSAGE_MAP()

void CPS4ControllerDlg::OnTimer(UINT_PTR nIDEvent)
{
	double x_left = ctrl.GetState().left_stick_axis_x;
	double y_left = ctrl.GetState().left_stick_axis_y;

	double x_right = ctrl.GetState().right_stick_axis_x;
	double y_right = ctrl.GetState().right_stick_axis_y;

	bool left_stick_click = ctrl.GetState().left_stick_click;
	bool right_stick_click = ctrl.GetState().right_stick_click;

	DrawPosForStick(PicDcLeftStick, PicLeftStick, x_left, y_left, left_stick_click);
	DrawPosForStick(PicDcRightStick, PicRightStick, x_right, y_right, right_stick_click);

	bool cross = ctrl.GetState().cross;
	bool trian = ctrl.GetState().triangle;
	bool rectan = ctrl.GetState().square;
	bool circle = ctrl.GetState().circle;

	DPADState updownleftright = ctrl.GetState().dpad;

	DrawStatusFigures(PicDcFiguresButton, PicFiguresButton, cross, trian, circle, rectan);
	DrawStatusArrow(PicDcArrowButton, PicArrowButton, updownleftright);

	int L2 = 255 - ctrl.GetState().L2;
	int R2 = 255 - ctrl.GetState().R2;

	progress_left_trigger.SetPos(L2);
	progress_right_trigger.SetPos(R2);

	int GyroX = ctrl.GetState().GyroX;
	int GyroY = ctrl.GetState().GyroY;
	int GyroZ = ctrl.GetState().GyroZ;

	vecGyroX.push_back(GyroX);
	vecGyroY.push_back(GyroY);
	vecGyroZ.push_back(GyroZ);

	if (vecGyroX.size() == 500 && vecGyroY.size() == 500 && vecGyroZ.size() == 500)
	{
		vecGyroX.erase(vecGyroX.begin(), vecGyroX.begin() + 1);
		vecGyroY.erase(vecGyroY.begin(), vecGyroY.begin() + 1);
		vecGyroZ.erase(vecGyroZ.begin(), vecGyroZ.begin() + 1);
	}

	DrawGyro(vecGyroX, vecGyroY, vecGyroZ, PicDcGyro, PicGyro);

	int AccelX = ctrl.GetState().AccelX;
	int AccelY = ctrl.GetState().AccelY;
	int AccelZ = ctrl.GetState().AccelZ;

	vecAccelX.push_back(AccelX);
	vecAccelY.push_back(AccelY);
	vecAccelZ.push_back(AccelZ);

	if (vecAccelX.size() == 500 && vecAccelY.size() == 500 && vecAccelZ.size() == 500)
	{
		vecAccelX.erase(vecAccelX.begin(), vecAccelX.begin() + 1);
		vecAccelY.erase(vecAccelY.begin(), vecAccelY.begin() + 1);
		vecAccelZ.erase(vecAccelZ.begin(), vecAccelZ.begin() + 1);
	}

	DrawAccel(vecAccelX, vecAccelY, vecAccelZ, PicDcAccel, PicAccel);

	int share = ctrl.GetState().share;
	bool isPs = ctrl.GetState().ps_button;
	int L1R1 = ctrl.GetState().L1R1;
	DrawButtons(PicDcButton, PicButton, share, isPs, L1R1);

	int firstX = ctrl.GetState().x;
	int firstY = ctrl.GetState().y;

	int secondX = ctrl.GetState().xx;
	int secondY = ctrl.GetState().yy;
	TPAD(secondX, secondY, firstX, firstY, PicDcTPAD, PicTPAD);

	CDialog::OnTimer(nIDEvent);
}

// Обработчики сообщений CPS4ControllerDlg

BOOL CPS4ControllerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	PicWndLeftStick = GetDlgItem(IDC_LEFT_STICK_PICTURE);			//связываем с ID окон
	PicDcLeftStick = PicWndLeftStick->GetDC();
	PicWndLeftStick->GetClientRect(&PicLeftStick);

	PicWndRightStick = GetDlgItem(IDC_RIGHT_STICK_PICTURE);			//связываем с ID окон
	PicDcRightStick = PicWndRightStick->GetDC();
	PicWndRightStick->GetClientRect(&PicRightStick);

	PicWndFiguresButton = GetDlgItem(IDC_FIGURES_BUTTONS);			//связываем с ID окон
	PicDcFiguresButton = PicWndFiguresButton->GetDC();
	PicWndFiguresButton->GetClientRect(&PicFiguresButton);

	PicWndArrowButton = GetDlgItem(IDC_CROSSHAIR);			//связываем с ID окон
	PicDcArrowButton = PicWndArrowButton->GetDC();
	PicWndArrowButton->GetClientRect(&PicArrowButton);

	PicWndGyro = GetDlgItem(IDC_GYROXYZ);			//связываем с ID окон
	PicDcGyro = PicWndGyro->GetDC();
	PicWndGyro->GetClientRect(&PicGyro);

	PicWndAccel = GetDlgItem(IDC_ACCELXYZ);			//связываем с ID окон
	PicDcAccel = PicWndAccel->GetDC();
	PicWndAccel->GetClientRect(&PicAccel);

	PicWndTPAD = GetDlgItem(IDC_TPad);			//связываем с ID окон
	PicDcTPAD = PicWndTPAD->GetDC();
	PicWndTPAD->GetClientRect(&PicTPAD);

	PicWndButton = GetDlgItem(IDC_BUTTON_PICTURE);			//связываем с ID окон
	PicDcButton = PicWndButton->GetDC();
	PicWndButton->GetClientRect(&PicButton);

	progress_left_trigger.SetRange(0, 255);
	progress_right_trigger.SetRange(0, 255);
	progress_left_trigger.SetPos(255);
	progress_right_trigger.SetPos(255);

	UpdateData(false);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CPS4ControllerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CPS4ControllerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CPS4ControllerDlg::Mashtab(double arr[], int dim, double* mmin, double* mmax)
{
	*mmin = *mmax = arr[0];

	for (int i = 0; i < dim; i++)
	{
		if (*mmin > arr[i]) *mmin = arr[i];
		if (*mmax < arr[i]) *mmax = arr[i];
	}
}

DWORD WINAPI MyProc(PVOID pv)
{
	CPS4ControllerDlg* p = (CPS4ControllerDlg*)pv;
	p->ReadFunction();
	return 0;
}

void CPS4ControllerDlg::DrawGyro(vector<int>& x, vector<int>& y, vector<int>& z, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА

	//область построения
	xmingyro = 0;			//минимальное значение х
	xmaxgyro = 500;			//максимальное значение х
	ymingyro = -1000;			//минимальное значение y
	ymaxgyro = 66000;		//максимальное значение y

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpgyro = (widthX / (xmaxgyro - xmingyro));			//Коэффициенты пересчёта координат по Х
	ypgyro = -(heightY / (ymaxgyro - ymingyro));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(240, 240, 240));

	CPen Gyrox;
	Gyrox.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(255, 0, 0));

	CPen Gyroy;
	Gyroy.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(0, 255, 0));

	CPen Gyroz;
	Gyroz.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(0, 0, 255));

	if (x.size() > 2)
	{
		for (int i = 3; i < x.size(); i++)
		{
			// отрисовка
			MemDc->MoveTo(DOTS_GYRO(i - 1, x[i - 1]));
			MemDc->SelectObject(&Gyrox);
			MemDc->LineTo(DOTS_GYRO(i, x[i]));

			// отрисовка
			MemDc->MoveTo(DOTS_GYRO(i - 1, y[i - 1]));
			MemDc->SelectObject(&Gyroy);
			MemDc->LineTo(DOTS_GYRO(i, y[i]));

			// отрисовка
			MemDc->MoveTo(DOTS_GYRO(i - 1, z[i - 1]));
			MemDc->SelectObject(&Gyroz);
			MemDc->LineTo(DOTS_GYRO(i, z[i]));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::DrawAccel(vector<int>& x, vector<int>& y, vector<int>& z, CDC* WinDc, CRect WinxmaxGraphc)
{
	//ГРАФИК СИГНАЛА

	//область построения
	xminaccel = 0;			//минимальное значение х
	xmaxaccel = 500;			//максимальное значение х
	yminaccel = -500;			//минимальное значение y
	ymaxaccel = 75000;		//максимальное значение y

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width();
	double heightY = WinxmaxGraphc.Height();
	xpaccel = (widthX / (xmaxaccel - xminaccel));			//Коэффициенты пересчёта координат по Х
	ypaccel = -(heightY / (ymaxaccel - yminaccel));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(240, 240, 240));

	CPen Accelx;
	Accelx.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(255, 0, 0));

	CPen Accely;
	Accely.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(0, 255, 0));

	CPen Accelz;
	Accelz.CreatePen(		//для сетки
		PS_SOLID,
		1,
		RGB(0, 0, 255));

	if (x.size() > 2)
	{
		for (int i = 3; i < x.size(); i++)
		{
			// отрисовка
			MemDc->MoveTo(DOTS_ACCEL(i - 1, x[i - 1]));
			MemDc->SelectObject(&Accelx);
			MemDc->LineTo(DOTS_ACCEL(i, x[i]));

			// отрисовка
			MemDc->MoveTo(DOTS_ACCEL(i - 1, y[i - 1]));
			MemDc->SelectObject(&Accely);
			MemDc->LineTo(DOTS_ACCEL(i, y[i]));

			// отрисовка
			MemDc->MoveTo(DOTS_ACCEL(i - 1, z[i - 1]));
			MemDc->SelectObject(&Accelz);
			MemDc->LineTo(DOTS_ACCEL(i, z[i]));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::DrawPosForStick(CDC* WinDc, CRect WinPic, double x, double y, bool isClick)
{
	ymax_left = 255 * 1.05;
	ymin_left = 0;
	xmax_left = 255 * 1.05;
	xmin_left = 0;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double window_signal_width = WinPic.Width();
	double window_signal_height = WinPic.Height();
	xp_left = (window_signal_width / (xmax_left - xmin_left));			//Коэффициенты пересчёта координат по Х
	yp_left = -(window_signal_height / (ymax_left - ymin_left));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, window_signal_width, window_signal_height);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinPic, RGB(240, 240, 240));

	CPen crossWithoutClick;
	crossWithoutClick.CreatePen(
		PS_SOLID,
		1,
		RGB(0, 0, 0));

	CPen crossWithClick;
	crossWithClick.CreatePen(
		PS_SOLID,
		4,
		RGB(155, 0, 0));

	double x_center = (xmin_left + xmax_left) / 2.;
	double y_center = (255 - ymin_left + ymax_left) / 2.;

	double radius = 0.055 * xmax_left;


	//создадим перекрестие в исходном положении
	MemDc->SelectObject(&crossWithoutClick);
	//отрисуем границы области
	MemDc->MoveTo(DOTS_STICK(0, ymin_left));
	MemDc->LineTo(DOTS_STICK(0, ymax_left));
	MemDc->MoveTo(DOTS_STICK(xmax_left - 1, ymin_left));
	MemDc->LineTo(DOTS_STICK(xmax_left - 1, ymax_left));
	MemDc->MoveTo(DOTS_STICK(0, ymin_left + 1));
	MemDc->LineTo(DOTS_STICK(xmax_left, ymin_left + 1));
	MemDc->MoveTo(DOTS_STICK(0, ymax_left));
	MemDc->LineTo(DOTS_STICK(xmax_left, ymax_left));
	if (isConnected == false)
	{
		//горизонтальная часть перекрестия
		MemDc->MoveTo(DOTS_STICK(x_center - radius, 255 - y_center));
		MemDc->LineTo(DOTS_STICK(x_center + radius, 255 - y_center));
		//вертикальная часть перекрестия
		MemDc->MoveTo(DOTS_STICK(x_center, 255 - y_center - radius));
		MemDc->LineTo(DOTS_STICK(x_center, 255 - y_center + radius));
	}
	else
	{
		if (!isClick)
		{
			//горизонтальная часть перекрестия
			MemDc->SelectObject(&crossWithoutClick);
			MemDc->MoveTo(DOTS_STICK(x - radius, 255 - y));
			MemDc->LineTo(DOTS_STICK(x + radius, 255 - y));
			//вертикальная часть перекрестия
			MemDc->MoveTo(DOTS_STICK(x, 255 - y - radius));
			MemDc->LineTo(DOTS_STICK(x, 255 - y + radius));
		}
		else
		{
			//горизонтальная часть перекрестия
			MemDc->SelectObject(&crossWithClick);
			MemDc->MoveTo(DOTS_STICK(x - radius, 255 - y));
			MemDc->LineTo(DOTS_STICK(x + radius, 255 - y));
			//вертикальная часть перекрестия
			MemDc->MoveTo(DOTS_STICK(x, 255 - y - radius));
			MemDc->LineTo(DOTS_STICK(x, 255 - y + radius));
		}
	}
	// вывод на экран
	WinDc->BitBlt(0, 0, window_signal_width, window_signal_height, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::DrawStatusFigures(CDC* WinDc, CRect WinPic, bool cross, bool trian, bool circle, bool rectan)
{
	ymax_figures = 100;
	ymin_figures = 0;
	xmax_figures = 100;
	xmin_figures = 0;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double window_signal_width = WinPic.Width();
	double window_signal_height = WinPic.Height();
	xp_figures = (window_signal_width / (xmax_figures - xmin_figures));			//Коэффициенты пересчёта координат по Х
	yp_figures = -(window_signal_height / (ymax_figures - ymin_figures));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, window_signal_width, window_signal_height);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinPic, RGB(240, 240, 240));

	CPen figures;
	figures.CreatePen(		//для сетки
		PS_SOLID,
		2,
		RGB(0, 0, 0));

	//создадим перекрестие в исходном положении
	MemDc->SelectObject(&figures);
	//отрисуем границы области
	MemDc->MoveTo(DOTS_FIGURES(0, ymin_figures));
	MemDc->LineTo(DOTS_FIGURES(0, ymax_figures));
	MemDc->MoveTo(DOTS_FIGURES(xmax_figures, ymin_figures));
	MemDc->LineTo(DOTS_FIGURES(xmax_figures, ymax_figures));
	MemDc->MoveTo(DOTS_FIGURES(0, ymin_figures));
	MemDc->LineTo(DOTS_FIGURES(xmax_figures, ymin_figures));
	MemDc->MoveTo(DOTS_FIGURES(0, ymax_figures));
	MemDc->LineTo(DOTS_FIGURES(xmax_figures, ymax_figures));
	if (isConnected == false)
	{

	}
	else
	{
		double x_center = (xmin_figures + xmax_figures) / 2.;
		double y_center = (ymin_figures + ymax_figures) / 2.;

		double cross_x_center = x_center;
		double cross_y_center = (y_center + ymin_figures) / 2.;

		double triangle_x_center = x_center;
		double triangle_y_center = (y_center + ymax_figures) / 2.;

		double rectangle_x_center = (x_center + xmin_figures) / 2.;
		double rectangle_y_center = y_center;

		double circle_x_center = (x_center + xmax_figures) / 2.;
		double circle_y_center = y_center;

		double up_x_center = x_center;
		double up_y_center = (y_center + ymin_figures) / 2.;

		double down_x_center = x_center;
		double down_y_center = (y_center + ymax_figures) / 2.;

		double left_x_center = (x_center + xmin_figures) / 2.;
		double left_y_center = y_center;

		double right_x_center = (x_center + xmax_figures) / 2.;
		double right_y_center = y_center;

		CBrush brush_down_cross(RGB(102, 153, 255));
		CBrush brush_down_rectan(RGB(255, 102, 255));
		CBrush brush_down_trian(RGB(0, 204, 102));
		CBrush brush_down_circle(RGB(255, 80, 80));
		CBrush brush_up(RGB(255, 255, 255));

		if (!cross)
		{
			//MemDc->SelectObject(&figures);
			//рисуем крестик
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_FIGURES(cross_x_center - 0.125 * xmax_figures, cross_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(cross_x_center + 0.125 * xmax_figures, cross_y_center + 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(cross_x_center + 7 - 0.125 * xmax_figures, cross_y_center - 7 + 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(cross_x_center - 7 + 0.125 * xmax_figures, cross_y_center + 7 - 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(cross_x_center + 7 - 0.125 * xmax_figures, cross_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(cross_x_center - 7 + 0.125 * xmax_figures, cross_y_center - 7 + 0.125 * ymax_figures));
		}
		else if (cross)
		{
			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_cross);
			MemDc->Ellipse(DOTS_FIGURES(cross_x_center - 0.125 * xmax_figures, cross_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(cross_x_center + 0.125 * xmax_figures, cross_y_center + 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(cross_x_center + 7 - 0.125 * xmax_figures, cross_y_center - 7 + 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(cross_x_center - 7 + 0.125 * xmax_figures, cross_y_center + 7 - 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(cross_x_center + 7 - 0.125 * xmax_figures, cross_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(cross_x_center - 7 + 0.125 * xmax_figures, cross_y_center - 7 + 0.125 * ymax_figures));
		}

		if (!trian)
		{
			MemDc->SelectObject(&figures);
			//рисуем треугольник
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_FIGURES(triangle_x_center - 0.125 * xmax_figures, triangle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(triangle_x_center + 0.125 * xmax_figures, triangle_y_center + 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(triangle_x_center, triangle_y_center - 7 + 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center - 7 + 0.125 * xmax_figures, triangle_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center + 7 - 0.125 * xmax_figures, triangle_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center, triangle_y_center - 7 + 0.125 * ymax_figures));
		}
		else
		{
			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_trian);
			MemDc->Ellipse(DOTS_FIGURES(triangle_x_center - 0.125 * xmax_figures, triangle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(triangle_x_center + 0.125 * xmax_figures, triangle_y_center + 0.125 * ymax_figures));
			MemDc->MoveTo(DOTS_FIGURES(triangle_x_center, triangle_y_center - 7 + 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center - 7 + 0.125 * xmax_figures, triangle_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center + 7 - 0.125 * xmax_figures, triangle_y_center + 7 - 0.125 * ymax_figures));
			MemDc->LineTo(DOTS_FIGURES(triangle_x_center, triangle_y_center - 7 + 0.125 * ymax_figures));
		}

		if (!rectan)
		{
			MemDc->SelectObject(&figures);
			//рисуем квадрат
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_FIGURES(rectangle_x_center - 0.125 * xmax_figures, rectangle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(rectangle_x_center + 0.125 * xmax_figures, rectangle_y_center + 0.125 * ymax_figures));
			MemDc->Rectangle(DOTS_FIGURES(rectangle_x_center - 0.04 * xmax_figures, rectangle_y_center - 0.07 * ymax_figures),
				DOTS_FIGURES(rectangle_x_center + 0.06 * xmax_figures, rectangle_y_center + 0.05 * ymax_figures));
		}
		else
		{
			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_rectan);
			MemDc->Ellipse(DOTS_FIGURES(rectangle_x_center - 0.125 * xmax_figures, rectangle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(rectangle_x_center + 0.125 * xmax_figures, rectangle_y_center + 0.125 * ymax_figures));
			MemDc->Rectangle(DOTS_FIGURES(rectangle_x_center + 7 - 0.125 * xmax_figures, rectangle_y_center - 7 + 0.125 * ymax_figures),
				DOTS_FIGURES(rectangle_x_center - 7 + 0.125 * xmax_figures, rectangle_y_center + 7 - 0.125 * ymax_figures));
		}

		if (!circle)
		{
			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			//рисуем круг
			MemDc->Ellipse(DOTS_FIGURES(circle_x_center - 0.125 * xmax_figures, circle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(circle_x_center + 0.125 * xmax_figures, circle_y_center + 0.125 * ymax_figures));
			MemDc->Ellipse(DOTS_FIGURES(circle_x_center + 7 - 0.125 * xmax_figures, circle_y_center + 7 - 0.125 * ymax_figures),
				DOTS_FIGURES(circle_x_center - 7 + 0.125 * xmax_figures, circle_y_center - 7 + 0.125 * ymax_figures));
		}
		else
		{
			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_circle);
			MemDc->Ellipse(DOTS_FIGURES(circle_x_center - 0.125 * xmax_figures, circle_y_center - 0.125 * ymax_figures),
				DOTS_FIGURES(circle_x_center + 0.125 * xmax_figures, circle_y_center + 0.125 * ymax_figures));
			MemDc->Ellipse(DOTS_FIGURES(circle_x_center + 7 - 0.125 * xmax_figures, circle_y_center + 7 - 0.125 * ymax_figures),
				DOTS_FIGURES(circle_x_center - 7 + 0.125 * xmax_figures, circle_y_center - 7 + 0.125 * ymax_figures));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, window_signal_width, window_signal_height, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::DrawStatusArrow(CDC* WinDc, CRect WinPic, DPADState updownleftright)
{
	ymax_arrow = 100;
	ymin_arrow = 0;
	xmax_arrow = 100;
	xmin_arrow = 0;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double window_signal_width = WinPic.Width();
	double window_signal_height = WinPic.Height();
	xp_arrow = (window_signal_width / (xmax_arrow - xmin_arrow));			//Коэффициенты пересчёта координат по Х
	yp_arrow = -(window_signal_height / (ymax_arrow - ymin_arrow));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, window_signal_width, window_signal_height);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinPic, RGB(240, 240, 240));

	CPen figures;
	figures.CreatePen(		//для сетки
		PS_SOLID,
		2,
		RGB(0, 0, 0));

	//создадим перекрестие в исходном положении
	MemDc->SelectObject(&figures);
	//отрисуем границы области
	MemDc->MoveTo(DOTS_ARROW(0, ymin_arrow));
	MemDc->LineTo(DOTS_ARROW(0, ymax_arrow));
	MemDc->MoveTo(DOTS_ARROW(xmax_arrow, ymin_arrow));
	MemDc->LineTo(DOTS_ARROW(xmax_arrow, ymax_arrow));
	MemDc->MoveTo(DOTS_ARROW(0, ymin_arrow));
	MemDc->LineTo(DOTS_ARROW(xmax_arrow, ymin_arrow));
	MemDc->MoveTo(DOTS_ARROW(0, ymax_arrow));
	MemDc->LineTo(DOTS_ARROW(xmax_arrow, ymax_arrow));

	if (isConnected == false)
	{

	}
	else
	{
		double x_center = (xmin_arrow + xmax_arrow) / 2.;
		double y_center = (ymin_arrow + ymax_arrow) / 2.;

		double up_x_center = x_center;
		double up_y_center = (y_center + ymin_arrow) / 2.;

		double down_x_center = x_center;
		double down_y_center = (y_center + ymax_arrow) / 2.;

		double left_x_center = (x_center + xmin_arrow) / 2.;
		double left_y_center = y_center;

		double right_x_center = (x_center + xmax_arrow) / 2.;
		double right_y_center = y_center;

		CBrush brush_down_up(RGB(255, 242, 0));
		CBrush brush_up(RGB(255, 255, 255));

		if (updownleftright == 0)
		{
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");

		}

		if (updownleftright == 1)
		{
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");
		}

		if (updownleftright == 2)
		{
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");

		}

		if (updownleftright == 3)
		{
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");
		}

		if (updownleftright == 4)
		{
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");
		}

		if (updownleftright == 5)
		{
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");
		}

		if (updownleftright == 6)
		{
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");

		}

		if (updownleftright == 7)
		{
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_down_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");
		}

		if (updownleftright == 8)
		{
			//MemDc->SelectObject(&figures);
			//рисуем крестик
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(up_x_center - 0.125 * xmax_arrow, up_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(up_x_center + 0.125 * xmax_arrow, up_y_center + 0.125 * ymax_arrow));
			CFont fontgraph;
			MemDc->SetBkMode(TRANSPARENT);
			MemDc->SelectObject(&fontgraph);
			MemDc->TextOut(DOTS_ARROW(up_x_center + 8 - 0.125 * xmax_arrow, up_y_center - 3 + 0.125 * ymax_arrow), L"S");

			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(down_x_center - 0.125 * xmax_arrow, down_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(down_x_center + 0.125 * xmax_arrow, down_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(down_x_center + 7 - 0.125 * xmax_arrow, down_y_center - 3 + 0.125 * ymax_arrow), L"N");


			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(left_x_center - 0.125 * xmax_arrow, left_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(left_x_center + 0.125 * xmax_arrow, left_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(left_x_center + 5 - 0.125 * xmax_arrow, left_y_center - 3 + 0.125 * ymax_arrow), L"W");

			MemDc->SelectObject(&figures);
			MemDc->SelectObject(&brush_up);
			MemDc->Ellipse(DOTS_ARROW(right_x_center - 0.125 * xmax_arrow, right_y_center - 0.125 * ymax_arrow),
				DOTS_ARROW(right_x_center + 0.125 * xmax_arrow, right_y_center + 0.125 * ymax_arrow));
			MemDc->TextOut(DOTS_ARROW(right_x_center + 8 - 0.125 * xmax_arrow, right_y_center - 3 + 0.125 * ymax_arrow), L"E");

		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, window_signal_width, window_signal_height, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}
ofstream out("DPAD.txt");

void CPS4ControllerDlg::TPAD(int x, int y, int xx, int yy, CDC* WinDc, CRect WinPic)
{
	ymax_T_pad = 255;
	ymin_T_pad = -4;
	xmax_T_pad = 255;
	xmin_T_pad = -4;

	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);
	bmp.CreateCompatibleBitmap(WinDc, WinPic.Width(), WinPic.Height());
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	MemDc->FillSolidRect(WinPic, RGB(240, 240, 240));
	double window_signal_width = WinPic.Width();
	double window_signal_height = WinPic.Height();

	xp_T_pad = (window_signal_width / (xmax_T_pad - xmin_T_pad));			//Коэффициенты пересчёта координат по Х
	yp_T_pad = -(window_signal_height / (ymax_T_pad - xmin_T_pad));			//Коэффициенты пересчёта координат по У

	CBrush brush(RGB(150, 0, 0));

	double widthRect1 = 0.015 * xmax_T_pad;
	double heightRect1 = 0.05 * ymax_T_pad;
	MemDc->SelectObject(&brush);
	MemDc->Ellipse(DOTS_TPAD(x - widthRect1, y - heightRect1),
		DOTS_TPAD(x + widthRect1, heightRect1 + y));
	MemDc->Ellipse(DOTS_TPAD(xx - widthRect1, yy - heightRect1),
		DOTS_TPAD(xx + widthRect1, heightRect1 + yy));

	out << "1finger_x: " << x << "\t1finger_y: " << y << endl << "2finger_x: " << xx << "\t2finger_y: " << yy << endl;

	WinDc->BitBlt(0, 0, WinPic.Width(), WinPic.Height(), MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::DrawButtons(CDC* WinDc, CRect WinPic, int isClick, bool isPs, int L1R1)
{
	ymax_button = 255;
	ymin_button = 0;
	xmax_button = 255;
	xmin_button = 0;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double window_signal_width = WinPic.Width();
	double window_signal_height = WinPic.Height();
	xp_button = (window_signal_width / (xmax_button - xmin_button));			//Коэффициенты пересчёта координат по Х
	yp_button = -(window_signal_height / (ymax_button - ymin_button));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, window_signal_width, window_signal_height);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);
	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinPic, RGB(240, 240, 240));

	CPen button;
	button.CreatePen(		//для сетки
		PS_SOLID,
		2,
		RGB(0, 0, 0));

	//создадим перекрестие в исходном положении
	MemDc->SelectObject(&button);
	//отрисуем границы области
	MemDc->MoveTo(DOTS_BUTTONS(0, ymin_button));
	MemDc->LineTo(DOTS_BUTTONS(0, ymax_button));
	MemDc->MoveTo(DOTS_BUTTONS(xmax_button - 1, ymin_button));
	MemDc->LineTo(DOTS_BUTTONS(xmax_button - 1, ymax_button));
	MemDc->MoveTo(DOTS_BUTTONS(0, ymin_button + 1));
	MemDc->LineTo(DOTS_BUTTONS(xmax_button, ymin_button + 1));
	MemDc->MoveTo(DOTS_BUTTONS(0, ymax_button));
	MemDc->LineTo(DOTS_BUTTONS(xmax_button, ymax_button));
	if (isConnected)
	{
		CBrush brush_down(RGB(155, 0, 0));
		CBrush brush_up(RGB(240, 240, 240));
		//share
		if (isClick == 16)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = 2.5 * (ymax_button + ymin_button) / 3.;

			double widthRect1 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y - widthRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y + widthRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 3.5, rec1_y - widthRect1), L"Share");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec2_x = (xmax_button + xmin_button) / 1.2;
			double rec2_y = (ymax_button + ymin_button) / 1.2;

			double widthRect2 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec2_x - widthRect2, rec2_y - widthRect2),
				DOTS_BUTTONS(rec2_x + widthRect2, rec2_y + widthRect2));
			MemDc->TextOut(DOTS_BUTTONS((rec2_x + rec2_x - widthRect2) / 2.15, rec2_y - widthRect2), L"Option");
		}
		else if (isClick == 32)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = 2.5 * (ymax_button + ymin_button) / 3.;

			double widthRect1 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y - widthRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y + widthRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 3.5, rec1_y - widthRect1), L"Share");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec2_x = (xmax_button + xmin_button) / 1.2;
			double rec2_y = (ymax_button + ymin_button) / 1.2;

			double widthRect2 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec2_x - widthRect2, rec2_y - widthRect2),
				DOTS_BUTTONS(rec2_x + widthRect2, rec2_y + widthRect2));
			MemDc->TextOut(DOTS_BUTTONS((rec2_x + rec2_x - widthRect2) / 2.15, rec2_y - widthRect2), L"Option");
		}
		else if (isClick == 48)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = 2.5 * (ymax_button + ymin_button) / 3.;

			double widthRect1 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y - widthRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y + widthRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 3.5, rec1_y - widthRect1), L"Share");

			double rec2_x = (xmax_button + xmin_button) / 1.2;
			double rec2_y = (ymax_button + ymin_button) / 1.2;

			double widthRect2 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec2_x - widthRect2, rec2_y - widthRect2),
				DOTS_BUTTONS(rec2_x + widthRect2, rec2_y + widthRect2));
			MemDc->TextOut(DOTS_BUTTONS((rec2_x + rec2_x - widthRect2) / 2.15, rec2_y - widthRect2), L"Option");
		}
		else
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = 2.5 * (ymax_button + ymin_button) / 3.;

			double widthRect1 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y - widthRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y + widthRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 3.5, rec1_y - widthRect1), L"Share");

			double rec2_x = (xmax_button + xmin_button) / 1.2;
			double rec2_y = (ymax_button + ymin_button) / 1.2;

			double widthRect2 = 0.1 * xmax_button;

			MemDc->Rectangle(DOTS_BUTTONS(rec2_x - widthRect2, rec2_y - widthRect2),
				DOTS_BUTTONS(rec2_x + widthRect2, rec2_y + widthRect2));
			MemDc->TextOut(DOTS_BUTTONS((rec2_x + rec2_x - widthRect2) / 2.15, rec2_y - widthRect2), L"Option");
		}

		if (!isPs)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec1_x = (xmax_button + xmin_button) / 2.;
			double rec1_y = (ymax_button + ymin_button) / 1.5;

			double widthRect1 = 0.1 * xmax_button;
			double heightRect1 = 0.15 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 1.99, rec1_y - heightRect1), L"Ps");
		}
		else
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec1_x = (xmax_button + xmin_button) / 2.;
			double rec1_y = (ymax_button + ymin_button) / 1.5;

			double widthRect1 = 0.1 * xmax_button;
			double heightRect1 = 0.15 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((rec1_x + rec1_x - widthRect1) / 1.99, rec1_y - heightRect1), L"Ps");
		}
		if (L1R1 == 1)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = (ymax_button + ymin_button) / 3.5;

			double widthRect1 = 0.05 * xmax_button;
			double heightRect1 = 0.1 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(2 * rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((2.5 * rec1_x + rec1_x - widthRect1) / 1.8, rec1_y - 1.2 * heightRect1), L"L1");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec2_x = (xmax_button + xmin_button) / 1.5;
			double rec2_y = (ymax_button + ymin_button) / 3.5;

			/*double widthRect2 = 0.1 * xmax_button;
			double heightRect2 = 0.15 * ymax_button;*/

			MemDc->Ellipse(DOTS_BUTTONS(rec2_x - widthRect1, rec2_y + heightRect1),
				DOTS_BUTTONS(1.25 * rec2_x + widthRect1, rec2_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((0.9 * rec2_x + rec2_x - widthRect1) / 1.95, rec2_y - 1.2 * heightRect1), L"L2");
		}
		else if (L1R1 == 2)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = (ymax_button + ymin_button) / 3.5;

			double widthRect1 = 0.05 * xmax_button;
			double heightRect1 = 0.1 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(2 * rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((2.5 * rec1_x + rec1_x - widthRect1) / 1.8, rec1_y - 1.2 * heightRect1), L"L1");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec2_x = (xmax_button + xmin_button) / 1.5;
			double rec2_y = (ymax_button + ymin_button) / 3.5;

			/*double widthRect2 = 0.1 * xmax_button;
			double heightRect2 = 0.15 * ymax_button;*/

			MemDc->Ellipse(DOTS_BUTTONS(rec2_x - widthRect1, rec2_y + heightRect1),
				DOTS_BUTTONS(1.25 * rec2_x + widthRect1, rec2_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((0.9 * rec2_x + rec2_x - widthRect1) / 1.95, rec2_y - 1.2 * heightRect1), L"L2");
		}
		else if (L1R1 == 3)
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = (ymax_button + ymin_button) / 3.5;

			double widthRect1 = 0.05 * xmax_button;
			double heightRect1 = 0.1 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(2 * rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((2.5 * rec1_x + rec1_x - widthRect1) / 1.8, rec1_y - 1.2 * heightRect1), L"L1");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_down);

			double rec2_x = (xmax_button + xmin_button) / 1.5;
			double rec2_y = (ymax_button + ymin_button) / 3.5;

			/*double widthRect2 = 0.1 * xmax_button;
			double heightRect2 = 0.15 * ymax_button;*/

			MemDc->Ellipse(DOTS_BUTTONS(rec2_x - widthRect1, rec2_y + heightRect1),
				DOTS_BUTTONS(1.25 * rec2_x + widthRect1, rec2_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((0.9 * rec2_x + rec2_x - widthRect1) / 1.95, rec2_y - 1.2 * heightRect1), L"L2");
		}
		else /*if (L1R1 == 0)*/
		{
			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec1_x = 0.5 * (xmax_button + xmin_button) / 3.;
			double rec1_y = (ymax_button + ymin_button) / 3.5;

			double widthRect1 = 0.05 * xmax_button;
			double heightRect1 = 0.1 * ymax_button;

			MemDc->Ellipse(DOTS_BUTTONS(rec1_x - widthRect1, rec1_y + heightRect1),
				DOTS_BUTTONS(2 * rec1_x + widthRect1, rec1_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((2.5 * rec1_x + rec1_x - widthRect1) / 1.8, rec1_y - 1.2 * heightRect1), L"L1");

			MemDc->SelectObject(&button);
			MemDc->SelectObject(&brush_up);

			double rec2_x = (xmax_button + xmin_button) / 1.5;
			double rec2_y = (ymax_button + ymin_button) / 3.5;

			/*double widthRect2 = 0.1 * xmax_button;
			double heightRect2 = 0.15 * ymax_button;*/

			MemDc->Ellipse(DOTS_BUTTONS(rec2_x - widthRect1, rec2_y + heightRect1),
				DOTS_BUTTONS(1.25 * rec2_x + widthRect1, rec2_y - heightRect1));
			MemDc->TextOut(DOTS_BUTTONS((0.9 * rec2_x + rec2_x - widthRect1) / 1.95, rec2_y - 1.2 * heightRect1), L"L2");
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, window_signal_width, window_signal_height, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CPS4ControllerDlg::ReadFunction()
{
	ctrl.Connect();
	isConnected = true;
	while (1)
	{
		ctrl.Update();
	}
}

void CPS4ControllerDlg::OnBnClickedButtonConnect()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	if (!bRunTh)
	{
		if (hThread == NULL)
		{
			hThread = CreateThread(NULL, 0, MyProc, this, 0, &dwThread);
		}
		bRunTh = true;
	}
	else
	{
		MessageBox(L"Интерфейс уже подключен!", L"Внимание!", MB_OK | MB_ICONINFORMATION);
	}
	SetTimer(1, 10, NULL);
}


void CPS4ControllerDlg::OnBnClickedButtonDisconnect()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	TerminateThread(hThread, 0);		//убиваем поток
	CloseHandle(hThread);
	hThread = NULL;
	bRunTh = false;

	vecGyroX.clear();
	vecGyroY.clear();
	vecGyroZ.clear();

	vecAccelX.clear();
	vecAccelY.clear();
	vecAccelZ.clear();

	KillTimer(1);
	Invalidate();
}
